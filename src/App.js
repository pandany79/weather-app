import React from 'react';
import { Grid, Row, Col } from 'react-flexbox-grid';
import Paper from '@material-ui/core/Paper';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';
import ForcastDetailsContainer from './containers/ForcastDetailsContainer';
import LocationListContainer from './containers/LocationListContainer';
import './App.css';
import './styles/styles.css';
import './styles/weather-icons/css/weather-icons.min.css'

const cities = [
  'Belmont,us',
  'Sunnyvale,us',
  'Cancun,mx',
  'Mexico,mx'
];

const App = () => {
  return (
    <Grid>
      <Row>
        <AppBar position='sticky'>
          <Toolbar>
            <Typography
              variant='h5'
              color='inherit'
            >
              Weather App
            </Typography>
          </Toolbar>
        </AppBar>
      </Row>
      <Row>
        <Col
          xs={ 12 }
          md={ 6 }
        >
          <LocationListContainer cities={ cities }/>
        </Col>
        <Col
          xs={ 12 }
          md={ 6 }
        >
          <Paper elevation={ 4 }>
            <div className="details">
              <ForcastDetailsContainer />
            </div>
          </Paper>
        </Col>
      </Row>
    </Grid>
  );
}

export default App;