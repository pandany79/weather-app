import transformForecast from './../services/transformForecast';
import transformWeather from './../services/transformWeather';
import { getUrlForecastByCity, getUrlWeatherByCity } from './../services/getApiUrlByCity';

export const SET_CITY = 'SET_CITY';
export const SET_FORECAST_DATA = 'SET_FORECAST_DATA';
export const GET_CITY_WEATHER = 'GET_CITY_WEATHER';
export const SET_CITY_WEATHER = 'SET_CITY_WEATHER';

/**
* Action creator for setting the city.
* @param {String} value 
*/
const setCity = (payload) => ({
  type: SET_CITY,
  payload
});

const setForecastData = (payload) => ({
  type: SET_FORECAST_DATA,
  payload
});

const getCityWeather = (payload) => ({
  type: GET_CITY_WEATHER,
  payload
});

const setCityWeather = (payload) => ({
  type: SET_CITY_WEATHER,
  payload
})

export const setSelectedCity = (payload) => {
  return (dispatch, getstate) => {
    const apiForecast = getUrlForecastByCity(payload);

    // Activate an indicator into the State for Searching data.
    dispatch(setCity(payload));

    const state = getstate();
    const forecastDate = (state.cities[payload] || {}).forecastDate
    const currentDate = new Date();

    if (forecastDate && (currentDate - forecastDate) < (1 * 60 * 1000)) {
      return;
    }

    return fetch(apiForecast)
      .then(resolve => resolve.json())
      .then(data => {
        const forecastData = transformForecast(data);

        // Update the State with the fetch result.
        dispatch(setForecastData({
          city: payload,
          forecastData
        }))
      });
  };
};

export const setWeather = (payload) => {
  return dispatch => {
    payload.forEach(city => {
      dispatch(getCityWeather(city));
      const apiWeather = getUrlWeatherByCity(payload)

      fetch(apiWeather)
        .then(resolve => resolve.json())
        .then(data => {
          const weather = transformWeather(data);
          dispatch(setCityWeather({
            city,
            weather
          }));
        });
    })
  }
}
