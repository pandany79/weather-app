import React from 'react';
import PropTypes from 'prop-types';
import CircularProgress from '@material-ui/core/CircularProgress'
import ForecastItem from './ForecastItem';

const renderForcastItemDays = (forecastData) => {
  return forecastData.map(forecast => {
    const { weekDay, hour, data } = forecast
    return (
      <ForecastItem
        key={`${weekDay}${hour}`}
        weekDay={ weekDay }
        hour={ hour }
        data={data }
      />
    )
  })
}

const renderProgress = () => (
  <CircularProgress size={ 50 } />
);

const ForecastDetails = ({ city, forecastData }) => {
  return (
    <div>
      <h2 className='forecast-title'>
        Forecast Details for { city }
      </h2>
      { forecastData
          ? renderForcastItemDays(forecastData)
          : renderProgress()
      }
    </div>
  )
}

ForecastDetails.propTypes = {
  city: PropTypes.string.isRequired,
  forecastData: PropTypes.array
}

export default ForecastDetails;