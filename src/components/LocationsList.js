import React from 'react';
import PropTypes from 'prop-types';
import WeatherLocation from './WeatherLocation';

const LocationList = ({ cities, onSelectedLocation }) => {
  const handleLocationClick = (city) => {
    onSelectedLocation(city);
  };

  const renderLocations = (cities) => (
    cities.map(city => {
      return (
        <WeatherLocation
          key={ city.key }
          city={ city.name }
          onLocationClick={ () => handleLocationClick(city.name) }
          data={ city.data }
        />
      )
    })
  );

  return (
    <div className='location-list'>
      { renderLocations(cities) }
    </div>
  );
};

LocationList.propTypes = {
  cities: PropTypes.array.isRequired,
  onSelectedLocation: PropTypes.func
}

export default LocationList;