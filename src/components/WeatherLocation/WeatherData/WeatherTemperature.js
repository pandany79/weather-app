import React from 'react';
import WeatherIcons from 'react-weathericons';
import PropTypes from 'prop-types';
import './styles.css';
import { CLOUD, SUN, RAIN, SNOW, THUNDER, DRIZZLE } from './../../../constants/weathers';

const icons = {
  [CLOUD]: 'cloud',
  [SUN]: 'day-sunny',
  [RAIN]: 'rain',
  [SNOW]: 'snow',
  [THUNDER]: 'day-thunderstore',
  [DRIZZLE]: 'day-showers'
}

const getWeatherIcon = weatherState => {
  let icon = icons[weatherState];

  if (!icon) {
    icon = icons['sun']
  }

  return (
    <WeatherIcons
      name={ icon }
      size='2x'
      className="icon"
    />
  );
}

const WeatherTemperature = ({ temperature, weatherState }) => (
  <div className="weather-temperature-container">
    {
      getWeatherIcon(weatherState)
    }
    <span className="temperature">{temperature}</span>
    <span className="temperature-type">{`° C`}</span>
  </div>
);

WeatherTemperature.propTypes = {
  temperature: PropTypes.number.isRequired,
  weatherState: PropTypes.string.isRequired
};

export default WeatherTemperature;