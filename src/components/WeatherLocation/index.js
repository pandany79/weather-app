import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress'
import { PropTypes } from 'prop-types';
import Location from './Location';
import WeatherData from './WeatherData';
import './styles.css';

const WeatherLocation = ({ city, data, onLocationClick }) => {
  return (
    <div
      className="weather-location-container"
      onClick={ onLocationClick }
    >
      { data
        ?
          <div>
            <Location city={ city } />
            <WeatherData data={ data } />
          </div>
        : <CircularProgress size={ 50 } />
      }
    </div>
  )
}

WeatherLocation.propTypes = {
  city: PropTypes.string.isRequired,
  data: PropTypes.shape({
    temperature: PropTypes.number.isRequired,
    weatherState: PropTypes.string.isRequired,
    humidity: PropTypes.number.isRequired,
    wind: PropTypes.string.isRequired
  }),
  onLocationClick: PropTypes.func
}

export default WeatherLocation;