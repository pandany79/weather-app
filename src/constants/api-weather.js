const weatherParams = {
  urlWeather: 'https://api.openweathermap.org/data/2.5/weather',
  urlForecast: 'https://api.openweathermap.org/data/2.5/forecast',
  apiKey: '611b0c1710acc25e3e22068004b22c0f'
}

export default weatherParams;