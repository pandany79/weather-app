import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getForecastDataFromCities } from './../reducers';
import ForecastDetails from './../components/ForecastDetails';

const ForcastDetailsContainer = (props) => {
  const { city, forecastData } = props
  return (
    props.city &&
      <ForecastDetails
        city={ city }
        forecastData={ forecastData }
      />
  );
}

ForcastDetailsContainer.propTypes = {
  city: PropTypes.string.isRequired,
  forecastData: PropTypes.array
};

const mapStateToProps = (state) => ({
  city: state.city,
  forecastData: getForecastDataFromCities(state)
})

export default connect(mapStateToProps, null)(ForcastDetailsContainer);