import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { setSelectedCity, setWeather } from './../actions';
import { getWeatherCities, getCity } from './../reducers';
import LocationList from '../components/LocationsList';

class LocationListContainer extends Component {
  componentDidMount() {
    const { setWeather, setCity, cities, city } = this.props;
    setWeather(cities)
    setCity(city);
  }

  handleSelectedLocation = (city) => {
    this.props.setCity(city);
  }

  render() {
    return (
      <LocationList
        cities={ this.props.citiesWeather }
        onSelectedLocation={ this.handleSelectedLocation }
      />
    );
  }
}

LocationListContainer.propTypes = {
  setCity: PropTypes.func.isRequired,
  setWeather: PropTypes.func.isRequired,
  city: PropTypes.string.isRequired,
  cities: PropTypes.array.isRequired,
  citiesWeather: PropTypes.array
}

const mapStateToProps = (state) => ({
  citiesWeather: getWeatherCities(state),
  city: getCity(state)
});

const mapDispatchToPropsActions = (dispatch) => ({
  setCity: value => dispatch(setSelectedCity(value)),
  setWeather: cities => dispatch(setWeather(cities))
});

export default connect(mapStateToProps, mapDispatchToPropsActions)(LocationListContainer);