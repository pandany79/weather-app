import { createSelector } from 'reselect';
import toPairs from 'lodash.topairs';
import { SET_FORECAST_DATA, GET_CITY_WEATHER, SET_CITY_WEATHER } from './../actions';

export const cities = (state = {}, action) => {
  switch (action.type) {
    case SET_FORECAST_DATA: {
      const { city, forecastData } = action.payload;
      return {
        ...state,
        [city]: {
          ...state[city],
          forecastData,
          forecastDate: new Date()
        }
      }
    }
    case GET_CITY_WEATHER: {
      const city = action.payload;
      return {
        ...state,
        [city]: {
          ...state[city],
          weather: null
        }
      }
    }
    case SET_CITY_WEATHER: {
      const { city, weather } = action.payload;
      return {
        ...state,
        [city]: {
          ...state[city],
          weather
        }
      }
    }
    default:
      return state;
  }
}

export const getForecastDataFromCities = createSelector((cities, city) => (cities[city] || {}).forecastData, forecastData => forecastData);

const fromObjectToArray = (cities) => (toPairs(cities).map(([key, value]) => ({
  key,
  name: key,
  data: value.weather
})))
export const getWeatherCities = createSelector(state => fromObjectToArray(state), cities => cities);