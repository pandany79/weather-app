import weatherParams from '../constants/api-weather';

export const getUrlWeatherByCity = (city) => {
  const { urlWeather, apiKey } = weatherParams;
  return `${urlWeather}?q=${city}&appid=${apiKey}`;
}

export const getUrlForecastByCity = (city) => {
  const { urlForecast, apiKey } = weatherParams;
  return `${urlForecast}?q=${city}&appid=${apiKey}`;
}