import moment from 'moment';
import transformWeather from './transformWeather'

const transformForecast = (data) => (
  data.list.filter(item => {
    return (
      moment.unix(item.dt).hour() === 23 ||
      moment.unix(item.dt).hour() === 5 ||
      moment.unix(item.dt).hour() === 11
    )
  })
    .map(item => (
      {
        weekDay: moment.unix(item.dt).format('ddd'),
        hour: moment.unix(item.dt).hour(),
        data: transformWeather(item)
      }
    ))
);

export default transformForecast;